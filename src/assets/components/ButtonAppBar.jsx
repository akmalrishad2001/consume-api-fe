import * as React from 'react';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import Button from '@mui/material/Button';
import { createTheme } from '@mui/material';
import { useHover } from '@uidotdev/usehooks';


const theme = createTheme({
  palette:{
    primary:{
      main: process.env.REACT_APP_COLOR_PRIMARY
    }
  }
});

export default function ButtonAppBar() {
  

  return (
    <Box sx={{ flexGrow: 1 }}>
      <AppBar  theme={theme}  position="static">
        <Toolbar>
          <Typography variant="h6" component="div" sx={{ flexGrow: 1 }}>
            My App
          </Typography>
          <Button color="inherit">Login</Button>
        </Toolbar>
      </AppBar>
    </Box>
  );
}